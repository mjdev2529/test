<?php
  if($page == page_url('dashboard')){
    $dashboard = "active";
    $a_year = "";
    $classes = "";
    $users = "";
    $branches = "";
    $people = "";
    $modules = "";
  }else if($page == page_url('academic_year')){
    $dashboard = "";
    $a_year = "active";
    $classes = "";
    $users = "";
    $branches = "";
    $people = "";
    $modules = "";
  }else if($page == page_url('classes')){
    $dashboard = "";
    $a_year = "";
    $classes = "active";
    $users = "";
    $branches = "";
    $people = "";
    $modules = "";
  }else if($page == page_url('suppliers')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "active";
    $users = "";
    $branches = "";
    $people = "";
    $modules = "";
  }else if($page == page_url('users')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $users = "active";
    $branches = "";
    $people = "";
    $modules = "";
  }else if($page == page_url('branches')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $users = "";
    $branches = "active";
    $people = "";
    $modules = "";
  }else if($page == page_url('people')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $users = "";
    $branches = "";
    $people = "active";
    $modules = "";
  }else if($page == page_url('modules')){
    $dashboard = "";
    $sales = "";
    $products = "";
    $suppliers = "";
    $users = "";
    $branches = "";
    $people = "";
    $modules = "active";
  }
?>
<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
  <li class="nav-header">NAVIGATION</li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('dashboard')?>" class="nav-link <?=$dashboard?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Home
      </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('academic_year')?>" class="nav-link <?=$a_year?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Academic year
      </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('classes')?>" class="nav-link <?=$classes?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Classes
      </p>
    </a>
  </li>
  <?php if($_SESSION['role'] == 1){ ?>
    <li class="nav-item">
      <a href="index.php?page=<?=page_url('modules')?>" class="nav-link <?=$modules?>">
        <i class="nav-icon far fa-circle"></i>
        <p>
          Modules
        </p>
      </a>
    </li>
  <?php } ?>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('people')?>" class="nav-link <?=$people?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        People
      </p>
    </a>
  </li>
  <!-- <li class="nav-header">MASTER DATA</li> -->
  <!-- <li class="nav-item">
    <a href="index.php?page=<?=page_url('users')?>" class="nav-link <?=$users?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Users Management
      </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('customers')?>" class="nav-link <?=$customers?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Customers
      </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('suppliers')?>" class="nav-link <?=$suppliers?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Suppliers
      </p>
    </a>
  </li>
  <li class="nav-item">
    <a href="index.php?page=<?=page_url('products')?>" class="nav-link <?=$products?>">
      <i class="nav-icon far fa-circle"></i>
      <p>
        Products
      </p>
    </a>
  </li> -->
  <!-- <li class="nav-header">TRANSACTIONS</li> -->
</ul>