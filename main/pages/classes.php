<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Classes</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <div class="row">
            <?php if($_SESSION["role"] == 1){ ?>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Classes List</h5>
                    <div class="card-tools">
                      <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_class_md">
                        Add
                      </button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="delete_class()">
                        Delete
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="tbl_class" class="table table-condensed">
                      <thead>
                        <tr>
                          <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAllClass()"></th>
                          <th style="width: 10px">#</th>
                          <th>Name</th>
                          <th style="width: 100px">Code</th>
                          <th style="width: 100px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <?php }else{ ?>
              <div class="col-md-12">
                <div class="card">
                  <div class="card-header">
                    <h5 class="card-title">Classes List</h5>
                    <div class="card-tools">
                      <button type="button" class="btn btn-sm btn-success" data-toggle="modal" data-target="#add_st_class_md">
                        Add
                      </button>
                      <button type="button" class="btn btn-sm btn-danger" onclick="delete_student_class()">
                        Delete
                      </button>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body">
                    <table id="tbl_class_student" class="table table-condensed">
                      <thead>
                        <tr>
                          <th style="width: 10px"><input type="checkbox" id="checkAllClass" onclick="checkAllClass()"></th>
                          <th style="width: 10px">#</th>
                          <th>Name</th>
                          <th width="300px">Teacher Name</th>
                          <th style="width: 100px">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            <!-- /.col -->
            <?php } ?>
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <!-- Add Modal -->
    <div class="modal fade" id="add_class_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Class</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_class_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Class Name</label>
                  <input type="text" name="class_name" class="form-control" placeholder="Class Name">
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Edit Modal -->
    <div class="modal fade" id="edit_class_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Edit Subject</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="edit_class_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Class Name</label>
                  <input type="text" name="class_name" id="class_name" class="form-control" placeholder="Class Name">
                  <input type="hidden" name="class_id" id="class_id" >
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <!-- Add Modal -->
    <div class="modal fade" id="add_st_class_md" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Add Class</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <form id="add_st_class_form" method="POST" action="#">
              <div class="card-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Class Code</label>
                  <input type="text" name="class_code" class="form-control" placeholder="Class Code">
                </div>
              </div>

              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>

    <script type="text/javascript">
      $(document).ready( function(){
        get_class();
        get_class_student();
      });

      function get_class(){
        $("#tbl_class").DataTable().destroy();
        $("#tbl_class").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/classes_data.php",
          },
          "processing": true,
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          "columns": [
          {
            orderable: false,
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.class_id+"' name='cb_classes'>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "class_name"
          },
          {
            "data": "class_code"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark btn-block' onclick='edit_class("+row.class_id+")'>Edit class</button>"+
              "<button class='btn btn-sm btn-outline-dark btn-block' onclick='class_details("+row.class_id+")'>View subjects</button>";
            }
          }
          ]
        });
      }

      //teacher 
      function checkAllClass(){
        var x = $("#checkAllClass").is(":checked");
        if(x){
          $("input[name=cb_classes]").prop("checked", true);
        }else{
          $("input[name=cb_classes]").prop("checked", false);
        }
      }

      $("#add_class_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/class_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: New class was added.");
              $("#add_class_md").modal("hide");
              $("input").val("");
              $("textarea").val("");
              get_class();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function edit_class(cID){
        var url = "../ajax/class_details.php";
        $.ajax({
          type: "POST",
          url: url,
          data: {cID: cID},
          success: function(data){
            $("#edit_class_md").modal();
            var o = JSON.parse(data);
            $("#class_id").val(cID);
            $("#class_name").val(o.class_name);
          }
        });
      }

      $("#edit_class_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/class_update.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: Class details was updated.");
              $("#edit_class_md").modal("hide");
              $("input").val("");
              get_class();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_class(){
        var conf = confirm("Are you sure to delete selected?");
        if(conf){
          var classes = [];
          $("input[name=cb_classes]:checked").each( function(){
            classes.push($(this).val());
          });

          if(classes.length != 0){
            var url = "../ajax/class_delete.php";
            $.ajax({
              type: "POST",
              url: url,
              data: {cID: classes},
              success: function(data){
                if(data != 0){
                  alert("Success: Selected class/es was removed.");
                  get_class();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }else{
            alert("Warning: No data selected.");
          }
        }
      }

      function class_details(cID){
        window.location.href="index.php?page=<?=page_url('classes_details')?>&c_id="+cID+"&pF=classes";
      }

      //student

      function get_class_student(){
        $("#tbl_class_student").DataTable().destroy();
        $("#tbl_class_student").dataTable({
          "ajax": {
            "type": "POST",
            "url": "../ajax/datatables/classes_student_data.php",
          },
          "processing": true,
          "bPaginate": false,
          "bLengthChange": false,
          "bFilter": true,
          "bInfo": false,
          // "sort": false,
          "columns": [
          {
            "mRender": function(data, type, row){
              return "<input type='checkbox' value='"+row.sclass_id+"' name='cb_sclasses'>";
            }
          },
          {
            "data": "count"
          },
          {
            "data": "class_name"
          },
          {
            "data": "teacher_name"
          },
          {
            "mRender": function(data, type, row){
              return "<button class='btn btn-sm btn-outline-dark btn-block' onclick='class_details("+row.class_id+")'>View subjects</button>";
            }
          }
          ]
        });
      }

      $("#add_st_class_form").submit( function(e){
        e.preventDefault();
        var data = $(this).serialize();
        var url = "../ajax/class_student_add.php";
        $.ajax({
          type: "POST",
          url: url,
          data: data,
          success: function(data){
            if(data == 1){
              alert("Success: New class was added.");
              $("#add_st_class_md").modal("hide");
              $("input").val();
              get_class_student();
            }else if(data == 2){
              alert("Warning: Class was already added.");
              $("#add_st_class_md").modal("hide");
              $("input").val();
            }else{
              alert("Error: Something is wrong.");
            }
          }
        });
      });

      function delete_student_class(){
        var conf = confirm("Are you sure to delete selected?");
        if(conf){
          var classes = [];
          $("input[name=cb_sclasses]:checked").each( function(){
            classes.push($(this).val());
          });

          if(classes.length != 0){
            var url = "../ajax/class_student_delete.php";
            $.ajax({
              type: "POST",
              url: url,
              data: {cID: classes},
              success: function(data){
                if(data != 0){
                  alert("Success: Selected class/es was removed.");
                  get_class_student();
                }else{
                  alert("Error: Something is wrong.");
                }
              }
            });
          }else{
            alert("Warning: No data selected.");
          }
        }
      }
    </script>