<?php
  $count_classes = mysqli_num_rows(mysqli_query($conn, "SELECT class_id FROM tbl_classes WHERE added_by = '$_SESSION[uid]'"));
  $count_modules = mysqli_num_rows(mysqli_query($conn, "SELECT module_id FROM tbl_modules WHERE added_by = '$_SESSION[uid]'"));
  $count_students = mysqli_num_rows(mysqli_query($conn, "SELECT sc.sclass_id FROM tbl_classes c INNER JOIN tbl_classes_student sc ON c.class_id = sc.class_id WHERE c.added_by = '$_SESSION[uid]'"));
  $count_announcement = mysqli_num_rows(mysqli_query($conn, "SELECT announcement_id FROM tbl_announcement WHERE user_id = '$_SESSION[uid]'"));

?>
<!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row">
          <div class="col-sm-6">
            <h1>Home</h1>
          </div>
          <div class="col-sm-4 text-right h5 pt-2">
            <i class="far fa-user mr-1"></i> Welcome: <?=$_SESSION["name"];?> ( <?=$_SESSION["role"] == 1?"Teacher":"Student";?> )
          </div>
          <div class="col-sm-2 text-center h5 pt-2">
            <i class="far fa-calendar-alt mr-1"></i> <?=date("F d, Y");?>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-body">
          <?php if($_SESSION['role'] == 1){ ?>
          <div class="row">
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-light">
                <div class="inner">
                  <h3><?=$count_students?></h3>

                  <p>Students</p>
                </div>
                <div class="icon">
                  <i class="ion ion-bag"></i>
                </div>
                <a href="index.php?page=<?=page_url('people')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-light">
                <div class="inner">
                  <h3><?=$count_classes;?></h3>

                  <p>Classes</p>
                </div>
                <div class="icon">
                  <i class="ion ion-stats-bars"></i>
                </div>
                <a href="index.php?page=<?=page_url('classes')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-light">
                <div class="inner">
                  <h3><?=$count_modules?></h3>

                  <p>Modules</p>
                </div>
                <div class="icon">
                  <i class="ion ion-person-add"></i>
                </div>
                <a href="index.php?page=<?=page_url('modules')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-6">
              <!-- small box -->
              <div class="small-box bg-light">
                <div class="inner">
                  <h3><?=$count_announcement?></h3>

                  <p>Announcements</p>
                </div>
                <div class="icon">
                  <i class="ion ion-pie-graph"></i>
                </div>
                <a href="index.php?page=<?=page_url('announcement')?>" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div>
            <!-- ./col -->
          </div>
        <?php } ?>
          <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h3 class="text-center">Welcome to KiMoMon: Kinder Module Monitoring</h3>
                </div>
                <p class="text-center col-8 offset-2 mt-5">
                </p>
                <!-- /.card-header -->
                <div class="card-body">
                </div>
              </div>
              <!-- /.card -->
            </div>
            <!-- /.col -->
          </div>
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>

    <script type="text/javascript">
    </script>