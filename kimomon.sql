-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 02, 2022 at 04:29 PM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kimomon`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_announcement`
--

CREATE TABLE `tbl_announcement` (
  `announcement_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `announcement` mediumtext NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_announcement`
--

INSERT INTO `tbl_announcement` (`announcement_id`, `user_id`, `class_id`, `announcement`, `date_added`) VALUES
(1, 1, 7, 'test', '2021-12-14'),
(2, 1, 0, 'announcement test', '2021-12-18');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classes`
--

CREATE TABLE `tbl_classes` (
  `class_id` int(11) NOT NULL,
  `class_name` varchar(200) NOT NULL,
  `class_code` varchar(50) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_classes`
--

INSERT INTO `tbl_classes` (`class_id`, `class_name`, `class_code`, `added_by`, `date_added`) VALUES
(5, 'Kinder - Apple', '2SXNMBTA', 3, '2021-10-26'),
(6, 'Kinder - Bear', 'Y2ZYRUG2', 3, '2021-10-26'),
(7, 'Kinder - Apple', 'UFH0UQOQ', 1, '2021-11-13'),
(8, 'Kinder - Bears', 'CIXU36P3', 1, '2021-11-13');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_classes_student`
--

CREATE TABLE `tbl_classes_student` (
  `sclass_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `class_code` varchar(200) NOT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_classes_student`
--

INSERT INTO `tbl_classes_student` (`sclass_id`, `class_id`, `class_code`, `added_by`) VALUES
(2, 7, 'UFH0UQOQ', 2),
(3, 8, 'CIXU36P3', 4);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_comment`
--

CREATE TABLE `tbl_comment` (
  `comment_id` int(11) NOT NULL,
  `comment` mediumtext NOT NULL,
  `module_id` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_comment`
--

INSERT INTO `tbl_comment` (`comment_id`, `comment`, `module_id`, `added_by`, `date_added`) VALUES
(1, 'test comment', 17, 2, '2022-01-02'),
(2, 'test comment 2', 17, 2, '2022-01-02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_lesson`
--

CREATE TABLE `tbl_lesson` (
  `lesson_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `lesson_name` text NOT NULL,
  `lesson_type` int(11) NOT NULL,
  `lesson_data` mediumtext NOT NULL,
  `added_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_lesson`
--

INSERT INTO `tbl_lesson` (`lesson_id`, `class_id`, `subject_id`, `lesson_name`, `lesson_type`, `lesson_data`, `added_by`) VALUES
(1, 7, 1, 'Test', 1, 'testlink.com', 1),
(2, 7, 1, 'Test', 2, '../assets/files/1-220101031233.pdf', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_modules`
--

CREATE TABLE `tbl_modules` (
  `module_id` int(11) NOT NULL,
  `subject_id` int(11) NOT NULL,
  `content_type` int(11) NOT NULL,
  `module_name` varchar(200) NOT NULL,
  `module_instructions` mediumtext NOT NULL,
  `module_deadline` date NOT NULL,
  `answer_type` int(11) NOT NULL,
  `is_posted` int(11) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_modules`
--

INSERT INTO `tbl_modules` (`module_id`, `subject_id`, `content_type`, `module_name`, `module_instructions`, `module_deadline`, `answer_type`, `is_posted`, `added_by`, `date_added`) VALUES
(1, 1, 1, 'test module', 'test instructions', '2021-12-22', 2, 1, 1, '2021-11-17'),
(8, 4, 1, 'test module 123', '', '2021-12-10', 2, 0, 1, '2021-11-30'),
(9, 1, 1, 'test module 2', 'test instructions', '2021-12-22', 1, 0, 1, '2021-12-20'),
(10, 1, 1, 'Basic Addition', 'Select the best answer to the problem.', '2022-01-03', 1, 1, 1, '2021-12-26'),
(11, 1, 1, 'Basic Subtraction', 'Select the best answer to problem.', '2022-01-03', 1, 0, 1, '2021-12-26'),
(15, 1, 2, 'test learning material', 'test learning material instructions', '0000-00-00', 0, 1, 1, '2021-12-31'),
(16, 1, 2, 'test', 'test', '0000-00-00', 0, 1, 1, '2021-12-31'),
(17, 1, 3, 'Activity', 'test', '2022-01-03', 0, 1, 1, '2022-01-02'),
(18, 1, 3, 'Activity 2', 'test', '2022-01-07', 0, 1, 1, '2022-01-02'),
(20, 4, 1, 'Basic Addition', 'Test Addition', '2022-01-07', 3, 1, 1, '2022-01-02');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_answer`
--

CREATE TABLE `tbl_module_answer` (
  `ma_id` int(11) NOT NULL,
  `mq_id` int(11) NOT NULL,
  `module_answer` mediumtext NOT NULL,
  `is_correct` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_module_answer`
--

INSERT INTO `tbl_module_answer` (`ma_id`, `mq_id`, `module_answer`, `is_correct`) VALUES
(1, 1, 'tq 1 a 1', 0),
(3, 1, 'tq 1 a 2', 0),
(4, 1, 'tq 1 a 3', 1),
(5, 5, 'tq 2 a 1', 0),
(8, 5, 'tq 2 a 2', 1),
(10, 5, 'tq 2 a 3', 0),
(11, 7, '0', 0),
(12, 7, '1', 0),
(13, 7, '2', 1),
(14, 8, '2', 0),
(15, 8, '3', 0),
(16, 8, '4', 1),
(20, 17, '../assets/files/2-2201021545180.pdf', 2),
(21, 17, '../assets/files/2-2201021545181.html', 2),
(26, 19, '1', 1),
(27, 19, '2', 1),
(28, 19, '3', 1),
(29, 20, '2', 1),
(30, 20, '3', 1),
(31, 20, '4', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_module_question`
--

CREATE TABLE `tbl_module_question` (
  `mq_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `module_question` mediumtext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_module_question`
--

INSERT INTO `tbl_module_question` (`mq_id`, `module_id`, `module_question`) VALUES
(1, 1, 'test question'),
(5, 1, 'test question 1'),
(7, 10, '1 + 1'),
(8, 10, '2 + 2'),
(15, 15, '../assets/files/1-2112310356410.pdf'),
(16, 15, '../assets/files/1-2112310356411.pdf'),
(17, 16, '../assets/files/1-2112310517480.pdf'),
(18, 16, '../assets/files/1-2112310517481.pdf'),
(21, 19, '1 + 1'),
(22, 19, '1  + 2 '),
(23, 19, '1 + 3'),
(24, 20, '1 + 1'),
(25, 20, '1 + 2'),
(26, 20, '1 + 3');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_schools`
--

CREATE TABLE `tbl_schools` (
  `school_id` int(11) NOT NULL,
  `school_name` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_schools`
--

INSERT INTO `tbl_schools` (`school_id`, `school_name`) VALUES
(1, 'test school');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_student_answers`
--

CREATE TABLE `tbl_student_answers` (
  `s_answer_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL,
  `mq_id` int(11) NOT NULL,
  `ma_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_student_answers`
--

INSERT INTO `tbl_student_answers` (`s_answer_id`, `user_id`, `module_id`, `mq_id`, `ma_id`) VALUES
(1, 2, 10, 7, 13),
(2, 2, 10, 8, 16),
(3, 2, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_subject`
--

CREATE TABLE `tbl_subject` (
  `subject_id` int(11) NOT NULL,
  `class_id` int(11) NOT NULL,
  `subject_name` varchar(200) NOT NULL,
  `added_by` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_subject`
--

INSERT INTO `tbl_subject` (`subject_id`, `class_id`, `subject_name`, `added_by`, `date_added`) VALUES
(1, 7, 'test subject', 1, '2021-11-13'),
(4, 8, 'test subject 1', 1, '2021-11-17');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `user_id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(100) NOT NULL,
  `age` int(11) NOT NULL DEFAULT 0,
  `bdate` date DEFAULT NULL,
  `sex` int(11) NOT NULL DEFAULT 0,
  `address` varchar(200) NOT NULL,
  `contact_no` varchar(11) NOT NULL,
  `school_id` int(11) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`user_id`, `name`, `username`, `password`, `age`, `bdate`, `sex`, `address`, `contact_no`, `school_id`, `role`) VALUES
(1, 'Admin', 'admin@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 25, '2021-11-30', 1, 'test address', '09123456789', 0, 1),
(2, 'student 1', 'student@email.com', '0cc175b9c0f1b6a831c399e269772661', 6, '2015-11-30', 1, 'test address', '09123456789', 0, 0),
(3, 'test', 'test@email.com', '81dc9bdb52d04dc20036dbd8313ed055', 0, NULL, 0, '', '', 1, 1),
(4, 'student 2', 'student2@email.com', '0cc175b9c0f1b6a831c399e269772661', 0, NULL, 0, '', '', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_viewed`
--

CREATE TABLE `tbl_viewed` (
  `view_id` int(11) NOT NULL,
  `announcement_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_added` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  ADD PRIMARY KEY (`announcement_id`);

--
-- Indexes for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  ADD PRIMARY KEY (`class_id`);

--
-- Indexes for table `tbl_classes_student`
--
ALTER TABLE `tbl_classes_student`
  ADD PRIMARY KEY (`sclass_id`);

--
-- Indexes for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  ADD PRIMARY KEY (`comment_id`);

--
-- Indexes for table `tbl_lesson`
--
ALTER TABLE `tbl_lesson`
  ADD PRIMARY KEY (`lesson_id`);

--
-- Indexes for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  ADD PRIMARY KEY (`module_id`);

--
-- Indexes for table `tbl_module_answer`
--
ALTER TABLE `tbl_module_answer`
  ADD PRIMARY KEY (`ma_id`);

--
-- Indexes for table `tbl_module_question`
--
ALTER TABLE `tbl_module_question`
  ADD PRIMARY KEY (`mq_id`);

--
-- Indexes for table `tbl_schools`
--
ALTER TABLE `tbl_schools`
  ADD PRIMARY KEY (`school_id`);

--
-- Indexes for table `tbl_student_answers`
--
ALTER TABLE `tbl_student_answers`
  ADD PRIMARY KEY (`s_answer_id`);

--
-- Indexes for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  ADD PRIMARY KEY (`subject_id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `tbl_viewed`
--
ALTER TABLE `tbl_viewed`
  ADD PRIMARY KEY (`view_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_announcement`
--
ALTER TABLE `tbl_announcement`
  MODIFY `announcement_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tbl_classes`
--
ALTER TABLE `tbl_classes`
  MODIFY `class_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tbl_classes_student`
--
ALTER TABLE `tbl_classes_student`
  MODIFY `sclass_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_comment`
--
ALTER TABLE `tbl_comment`
  MODIFY `comment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_lesson`
--
ALTER TABLE `tbl_lesson`
  MODIFY `lesson_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_modules`
--
ALTER TABLE `tbl_modules`
  MODIFY `module_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `tbl_module_answer`
--
ALTER TABLE `tbl_module_answer`
  MODIFY `ma_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `tbl_module_question`
--
ALTER TABLE `tbl_module_question`
  MODIFY `mq_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT for table `tbl_schools`
--
ALTER TABLE `tbl_schools`
  MODIFY `school_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tbl_student_answers`
--
ALTER TABLE `tbl_student_answers`
  MODIFY `s_answer_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_subject`
--
ALTER TABLE `tbl_subject`
  MODIFY `subject_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tbl_viewed`
--
ALTER TABLE `tbl_viewed`
  MODIFY `view_id` int(11) NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
