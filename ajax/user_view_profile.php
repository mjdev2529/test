<?php
	session_start();
	include '../core/config.php';

	$u_id = $_POST["uID"];

    $getStudentsProfile = mysqli_query($conn, "SELECT * FROM tbl_users WHERE user_id = '$u_id'");
    $hide_s = $_SESSION['role'] == 0?"style='display: none'":"";
?>
<div class="row">
        <?php
            if(mysqli_num_rows($getStudentsProfile) != 0){
            $row = mysqli_fetch_array($getStudentsProfile);
        ?>
            <div class="col-10 offset-1">
                <div class="row">
                    <div class="col-6">
                        <label>Name:</label>
                    </div>
                    <div class="col-6">
                        <?=strtoupper($row['name'])?>
                    </div>
                    
                    <div class="col-6">
                        <label>Birthdate:</label>
                    </div>
                    <div class="col-6">
                        <?=$row['bdate']?>
                    </div>
                    
                    <div class="col-6">
                        <label>Age:</label>
                    </div>
                    <div class="col-6">
                        <?=$row['age']?>
                    </div>

                    <div class="col-6">
                        <label>Sex:</label>
                    </div>
                    <div class="col-6">
                        <?=$row['sex']==1?"Male":"Female"?>
                    </div>
                    
                    <div class="col-6">
                        <label>Address:</label>
                    </div>
                    <div class="col-6">
                        <?=strtoupper($row['address'])?>
                    </div>

                    <div class="col-6">
                        <label>eMail:</label>
                    </div>
                    <div class="col-6">
                        <?=$row['username']?>
                    </div>

                    <div class="col-6">
                        <label>Contact No.:</label>
                    </div>
                    <div class="col-6">
                        <?=$row['contact_no']?>
                    </div>
                </div>
            </div>
        <?php }else{ ?>
            <div class="col-12 text-center">
                <h3>No data available</h3>
            </div>
        <?php } ?>
</div>