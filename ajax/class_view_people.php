<?php
	session_start();
	include '../core/config.php';

	$c_id = $_POST["cID"];

    $getStudents = mysqli_query($conn, "SELECT * FROM tbl_classes_student WHERE class_id = '$c_id'");
    $hide_s = $_SESSION['role'] == 0?"style='display: none'":"";
?>
<table id="tbl_class_people" class="table table-condensed table-bordered">
    <thead>
    <tr>
        <th style="width: 10px">#</th>
        <th>Name</th>
    </tr>
    </thead>
    <tbody>
        <?php
            if(mysqli_num_rows($getStudents) != 0){
            $count = 1;
            while($row = mysqli_fetch_array($getStudents)){
        ?>
            <tr>
                <td><?=$count++?></td>
                <td><?=getStudentName($conn, $row['added_by'])?></td>
                <td width="15px" <?=$hide_s?>><button class="btn btn-sm btn-outline-primary" onclick="viewProfile(<?=$row['added_by']?>)">View</button></td>
            </tr>
        <?php } }else{ ?>
            <tr>
                <td colspan="2" class="text-center">No data available</td>
            </tr>
        <?php } ?>
    </tbody>
</table>